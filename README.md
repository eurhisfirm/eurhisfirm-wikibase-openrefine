# eurhisfirm-wikibase-openrefine

## Context

[OpenRefine](https://openrefine.org/) is a powerful tool to work on datasets and is currently the main tool used to clean and import data on Wikidata. Recent changes to OpenRefine seem to make it possible to be used on custom Wikibase instances. In the context of [EurHisFirm](http://eurhisfirm.eu/), enabling the use of OpenRefine to import data from source databases (DFIH, SCOB) to our [Wikibase instance](http://data.eurhisfirm.eu/) could be a significant improvement, from custom Python scripts which are complicated and not flexible, and would be at least a great complement.

This will enable users to use OpenRefine to "[reconcile](https://github.com/OpenRefine/OpenRefine/wiki/Reconciliation)" as well as import their data.

## Reconciliation service

Install a Wikibase reconciliation interface ([`openrefine-wikibase`](https://github.com/wetneb/openrefine-wikibase)) to use OpenRefine with the [EurHisFirm Wikibase instance](http://data.eurhisfirm.eu/).

Get started on the server:
```
ssh root@data.eurhisfirm.eu

cd /etc/docker/compose/

git clone https://github.com/wetneb/openrefine-wikibase
```

Configure the service:
```
cp config_docker.py config.py

nano config.py
```

Current configuration can be copied from [`config.py`](/config.py).

Start the service:
```
docker-compose up // or docker-compose up -d for the detached mode (in the background)

```

Access the service: http://data.eurhisfirm.eu:8000/

## Wikibase manifest

[Write a manifest](https://github.com/OpenRefine/OpenRefine/wiki/Write-a-Wikibase-manifest) for the the [EurHisFirm Wikibase instance](http://data.eurhisfirm.eu/) so that OpenRefine can read how the instance is configured.

Current version can be copied from [`eurhisfirm-wikibase-manifest.json`](/eurhisfirm-wikibase-manifest.json).